//
//  ViewController.m
//  ProgressCircle
//
//  Created by ingwine on 9/2/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import "ViewController.h"
#import "GUIHelper.h"

@interface ViewController ()

@property (assign,nonatomic) float startValue;
@property (assign,nonatomic) float maxValue;
@property (strong, nonatomic) IBOutlet UISlider *slider;
@property (strong, nonatomic) IBOutlet ProgressCircle *progressCircle;
@property (strong, nonatomic) IBOutlet ProgressCircle *progressCircleWithTimer;
@property (strong, nonatomic) IBOutlet ProgressCircle *progressCircleWithWrongValues;
@property (strong, nonatomic) ProgressCircle *programmProgressCircle;
- (IBAction)valueChangedBySlider:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.programmProgressCircle = [[ProgressCircle alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    self.programmProgressCircle.center = self.view.center;
    [self.programmProgressCircle updateProgressValue:1];
    [self.view addSubview:self.programmProgressCircle];
    [self.programmProgressCircle updateBorderWidth:-1];
    
    [self.progressCircleWithTimer colorizeBackgroundWithColor:[UIColor orangeColor]];
    [self.progressCircleWithTimer colorizeBorderWithColor:[UIColor purpleColor]];
    [self.progressCircleWithTimer updateBorderWidth:30.0f];
    
    [self.progressCircle colorizeBackgroundWithColor:[UIColor blueColor]];
    [self.progressCircle colorizeSectorWithColor:[UIColor redColor]];
    [self.progressCircle updateProgressValue:0];
    [self.progressCircle colorizeBorderWithColor:[UIColor purpleColor]];
    [self.progressCircle updateBorderWidth:10.0f];
    
    self.slider.value = 0;
    self.startValue = 0;
    self.maxValue = 100;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [NSTimer scheduledTimerWithTimeInterval:0.1
                                     target:self
                                   selector:@selector(valueIncreased)
                                   userInfo:nil
                                    repeats:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:1
                                     target:self
                                   selector:@selector(changeColorsWithBorder)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)valueIncreased
{
    [self.progressCircleWithTimer updateProgressValue:self.startValue/self.maxValue];
    [self.progressCircleWithWrongValues updateProgressValue:self.maxValue/self.startValue];
    self.startValue += 1;
}

- (void)changeColorsWithBorder
{
    [self.progressCircle colorizeBorderWithColor: [GUIHelper generateRandomColor]];
    //[self.progressCircle colorizeBackgroundWithColor:[GUIHelper generateRandomColor]];
    [self.progressCircle colorizeSectorWithColor:[GUIHelper generateRandomColor]];
    [self.progressCircle updateBorderWidth:arc4random_uniform(20)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)valueChangedBySlider:(id)sender
{
    [self.progressCircle updateProgressValue:self.slider.value];
    [self.programmProgressCircle updateProgressValue:1 - self.slider.value];
}

@end
