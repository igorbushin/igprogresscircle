//
//  GUIHelper.m
//  ProgressCircle
//
//  Created by Developer on 06/09/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import "GUIHelper.h"

@implementation GUIHelper
+ (UIColor *)generateRandomColor
{
    CGFloat red    = rand() / (float) RAND_MAX;
    CGFloat green  = rand() / (float) RAND_MAX;
    CGFloat blue   = rand() / (float) RAND_MAX;
    UIColor *color = [[UIColor alloc] initWithRed:red green:green blue:blue alpha:1.0];
    return color;
}
@end
