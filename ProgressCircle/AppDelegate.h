//
//  AppDelegate.h
//  ProgressCircle
//
//  Created by ingwine on 9/2/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

