//
//  GUIHelper.h
//  ProgressCircle
//
//  Created by Developer on 06/09/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GUIHelper : NSObject
+ (UIColor *)generateRandomColor;
@end
