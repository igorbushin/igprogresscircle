//
//  ProgressCircle.m
//  ProgressCircle
//
//  Created by ingwine on 9/2/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import "ProgressCircle.h"
#import <CoreGraphics/CoreGraphics.h>

@interface ProgressCircle()

@property (assign,nonatomic) CGFloat progressValue;
@property (assign,nonatomic) CGFloat borderWidth;
@property (strong,nonatomic) UIColor *progressSectorColor;
@property (strong,nonatomic) UIColor *borderColor;

@end

@implementation ProgressCircle

- (instancetype)init
{
    self = [self initWithFrame:CGRectZero];
    if(self)
    {
        [self colorize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self colorize];
    }
    return self;
}

- (void)awakeFromNib
{
    [self colorize];
}

- (void)colorize
{
    self.progressSectorColor = [UIColor greenColor];
    self.backgroundColor = [UIColor grayColor];
}

- (void)colorizeSectorWithColor:(UIColor *)color
{
    self.progressSectorColor = color;
    [self setNeedsDisplay];
}

- (void)colorizeBackgroundWithColor:(UIColor *)color
{
    self.backgroundColor = color;
    [self setNeedsDisplay];
}

- (void)colorizeBorderWithColor:(UIColor*)color
{
    self.borderColor = color;
    [self setNeedsDisplay];
}

- (void)updateBorderWidth:(CGFloat)width
{
    if(width < 0)
    {
        NSLog(@"border width %f less then 0", width);
        self.borderWidth = 0;
    }
    else
    {
        self.borderWidth = width;
    }
    [self setNeedsDisplay];
}

- (void)updateProgressValue:(CGFloat)progressValue
{
    if(progressValue < 0.0)
    {
        NSLog(@"progress value %f less then minimum value",progressValue);
        self.progressValue = 0.0;
    }
    else if(progressValue > 1.0)
    {
        NSLog(@"progress value %f more then maximum value",progressValue);
        self.progressValue = 1.0;
    }
    else
    {
        self.progressValue = progressValue;
    }
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    CGPoint center = CGPointMake(rect.size.width / 2.0, rect.size.height / 2.0);
    CGFloat radius = MIN(rect.size.width, rect.size.height) / 2.0;
    CGFloat startAngle = -M_PI/2.0;
    CGFloat endAngle = -M_PI/2.0 + 2*M_PI*self.progressValue;
    
    
    //clip contour
    UIBezierPath *contour = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:0 endAngle:M_PI*2 clockwise:YES];
    CAShapeLayer *shape = [CAShapeLayer layer];
    shape.path = contour.CGPath;
    [self.layer setMask:shape];
    
    //draw progress as sector
    UIBezierPath *sectorPath = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:startAngle endAngle:endAngle clockwise:YES];
    [sectorPath addLineToPoint:center];
    [sectorPath closePath];
    [self.progressSectorColor setFill];
    [sectorPath fill];
    
    //draw border
    CGFloat safeBorderWidth = self.borderWidth;
    if(self.borderWidth > radius)
    {
        NSLog(@"border width %f greater then radius", self.borderWidth);
        safeBorderWidth = radius;
    }
    CGFloat borderRadius = radius - safeBorderWidth / 2.0;
    UIBezierPath *border = [UIBezierPath bezierPathWithArcCenter:center radius:borderRadius startAngle:0 endAngle:M_PI*2 clockwise:YES];
    [self.borderColor setStroke];
    border.lineWidth = safeBorderWidth;
    [border stroke];
}

@end
