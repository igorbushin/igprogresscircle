//
//  ProgressCircle.h
//  ProgressCircle
//
//  Created by ingwine on 9/2/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressCircle : UIView
- (void)updateProgressValue:(CGFloat)progressValue;
- (void)colorizeSectorWithColor:(UIColor*)color;
- (void)colorizeBackgroundWithColor:(UIColor*)color;
- (void)colorizeBorderWithColor:(UIColor*)color;
- (void)updateBorderWidth:(CGFloat)width;
@end
